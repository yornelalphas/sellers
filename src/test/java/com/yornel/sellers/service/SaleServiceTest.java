package com.yornel.sellers.service;

import com.yornel.sellers.model.Sale;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

@SpringBootTest
public class SaleServiceTest {

    @Autowired
    private SaleService service;

    @Test
    void getSales() {
        List<Sale> sales = service.getAll();
        Assertions.assertNotNull(sales);
    }

    @Test
    void getSalesBySellerIdExistent() {
        long sellerId = 1L;
        List<Sale> sales = service.getAllBySellerId(sellerId);
        Assertions.assertNotNull(sales);
    }

    @Test
    void getSalesBySellerIdNotExistent() {
        long sellerId = 5L;
        List<Sale> sales = service.getAllBySellerId(sellerId);
        Assertions.assertEquals(0, sales.size());
    }

    @Test
    void getSaleByIdFound() {
        long saleId = 8L;
        Sale sale = service.findById(saleId);
        Assertions.assertNotNull(sale);
    }

    @Test
    void getSaleByIdNotFound() {
        long saleId = 50000L;
        Sale sale = service.findById(saleId);
        Assertions.assertNull(sale);
    }

    @Test
    void saveDocumentSuccess() throws FileNotFoundException {
        String path = "src/test/resources/sales.csv";
        File file = new File(path);
        InputStream targetStream = new FileInputStream(file);
        service.saveAll(targetStream);
        Assertions.assertTrue(file.exists());
    }

    @Test
    void saveDocumentFailure() throws FileNotFoundException {
        String path = "src/test/resources/bad.csv";
        File file = new File(path);
        InputStream targetStream = new FileInputStream(file);
        service.saveAll(targetStream);
        Assertions.assertTrue(file.exists());
    }

    @Test
    void saveDocumentFailureByValue() throws FileNotFoundException {
        String path = "src/test/resources/badValue.csv";
        File file = new File(path);
        InputStream targetStream = new FileInputStream(file);
        service.saveAll(targetStream);
        Assertions.assertTrue(file.exists());
    }
}

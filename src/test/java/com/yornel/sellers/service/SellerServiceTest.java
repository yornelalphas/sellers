package com.yornel.sellers.service;

import com.yornel.sellers.model.Seller;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SellerServiceTest {

    @Autowired
    private SellerService service;

    @Test
    void getSellers() {
        List<Seller> sellers = service.getAll();
        Assertions.assertNotNull(sellers);
    }

    @Test
    void getByIdFound() {
        long testId = 1L;
        Seller seller = service.findById(testId);
        Assertions.assertNotNull(seller);
    }

    @Test
    void getByIdNotFound() {
        long testId = 5L;
        Seller seller = service.findById(testId);
        Assertions.assertNull(seller);
    }
}

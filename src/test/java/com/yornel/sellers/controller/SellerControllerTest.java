package com.yornel.sellers.controller;

import com.yornel.sellers.model.Seller;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class SellerControllerTest {

    @Autowired
    private SellerController controller;

    @Test
    void getSellers() {
        List<Seller> sellers = controller.getAll();
        Assertions.assertNotNull(sellers);
    }

    @Test
    void getByIdFound() {
        long testId = 1L;
        Seller seller = controller.findById(testId);
        Assertions.assertNotNull(seller);
    }

    @Test
    void getByIdNotFound() {
        long testId = 5L;
        Seller seller = controller.findById(testId);
        Assertions.assertNull(seller);
    }
}

package com.yornel.sellers.controller;

import com.yornel.sellers.model.Sale;
import com.yornel.sellers.model.Seller;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.List;

@SpringBootTest
public class SaleControllerTest {

    @Autowired
    private SaleController controller;

    @Test
    void getSales() {
        List<Sale> sales = controller.getAll();
        Assertions.assertNotNull(sales);
    }

    @Test
    void getSalesBySellerIdExistent() {
        long sellerId = 1L;
        List<Sale> sales = controller.getAllBySellerId(sellerId);
        Assertions.assertNotNull(sales);
    }

    @Test
    void getSalesBySellerIdNotExistent() {
        long sellerId = 5L;
        List<Sale> sales = controller.getAllBySellerId(sellerId);
        Assertions.assertEquals(0, sales.size());
    }

    @Test
    void getSaleByIdFound() {
        long saleId = 8L;
        Sale sale = controller.findById(saleId);
        Assertions.assertNotNull(sale);
    }

    @Test
    void getSaleByIdNotFound() {
        long saleId = 50000L;
        Sale sale = controller.findById(saleId);
        Assertions.assertNull(sale);
    }

    @Test
    void saveDocumentFailure() throws IOException {
        controller.upload(null);
    }
}

package com.yornel.sellers.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;

@SpringBootTest
public class DateUtilsTest {

	@Test
	public void getCurrentTimestamp() {
		Timestamp now = DateUtils.getCurrentTimestamp();
		Assertions.assertNotNull(now);
	}
	
	@Test
	public void isValidTimestampValid() {
		Assertions.assertTrue(DateUtils.isValidTimestamp("2020-02-01 12:12:12"));
	}
	
	@Test
	public void isValidTimestampError1() {
		Assertions.assertFalse(DateUtils.isValidTimestamp("-01 12:12:12"));
	}

	@Test
	public void isValidTimestampError2() {
		Assertions.assertFalse(DateUtils.isValidTimestamp(""));
	}

	@Test
	public void isValidTimestampError3() {
		Assertions.assertFalse(DateUtils.isValidTimestamp(null));
	}
	
	@Test
	public void toTimestampValid() {
		Assertions.assertNotNull(DateUtils.toTimestamp("2020-12-01 12:12:12"));
	}

	@Test
	public void toTimestampError() {
		Assertions.assertNull(DateUtils.toTimestamp("-01 12:12:12"));
	}
}

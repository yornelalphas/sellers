package com.yornel.sellers;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.task.TaskExecutor;

@SpringBootTest
class SellersApplicationTests {

	@Test
	void contextLoads() {
		TaskExecutor taskExecutor = new SellersApplication().getUploadAsyncExecutor();
		Assertions.assertNotNull(taskExecutor);
	}
}

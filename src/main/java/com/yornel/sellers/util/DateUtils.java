package com.yornel.sellers.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    private DateUtils() {

    }

    public static Timestamp getCurrentTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    public static Timestamp toTimestamp(String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT-5"));
        try {
            Date date =  dateFormat.parse(time);
            return new Timestamp(date.getTime());
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isValidTimestamp(String timestamp) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setLenient(false);
            sdf.parse(timestamp);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

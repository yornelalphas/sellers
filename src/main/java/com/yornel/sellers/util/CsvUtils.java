package com.yornel.sellers.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class CsvUtils {

    private CsvUtils() {

    }

    private static Logger logger = LoggerFactory.getLogger(CsvUtils.class);

    public static List<Map<String, String>> read(InputStream stream) {
        List<Map<String, String>> sales = new ArrayList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        String line = "";
        List<String> headerList = null;
        int count = 0;
        try {
            while ((line = br.readLine()) != null) {
                if (count > 0) {
                    Map<String, String> sale = new HashMap<>();
                    List<String> saleList = Arrays.asList(line.split("\\s*,\\s*"));
                    for (String header: headerList) {
                        try {
                            sale.put(header, saleList.get(headerList.indexOf(header)));
                        } catch (Exception e) {
                            sale.put(header, null);
                        }
                    }
                    sales.add(sale);
                } else {
                    headerList = Arrays.asList(line.split("\\s*,\\s*"));
                }
                count++;
            }
        } catch (IOException e) {
            logger.info("error parsing cvs document");
        }
        return sales;
    }
}

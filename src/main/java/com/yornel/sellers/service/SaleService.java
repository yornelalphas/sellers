package com.yornel.sellers.service;

import com.yornel.sellers.dto.SaleMapper;
import com.yornel.sellers.model.Sale;
import com.yornel.sellers.repository.SaleRepository;
import com.yornel.sellers.util.CsvUtils;
import com.yornel.sellers.validation.SaleValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

@Service
public class SaleService {

    @Autowired
    private SaleRepository repository;

    public List<Sale> getAll() {
        return repository.getAll();
    }

    public List<Sale> getAllBySellerId(Long sellerId) {
        return repository.getAllBySellerId(sellerId);
    }

    public Sale findById(Long saleId) {
        return repository.findById(saleId).orElse(null);
    }

    @Async("uploadExecutor")
    @Transactional
    public void saveAll(InputStream body) {
        if (body != null) {
            List<Map<String, String>> mapSales = CsvUtils.read(body);
            for (Map<String, String> mapSale : mapSales) {
                save(mapSale);
            }
        }
    }

    private int save(Map<String, String> mapSale) {
        if (SaleValidation.validate(mapSale)) {
            Sale sale = SaleMapper.mapToSale(mapSale);
            return repository.insert(
                    sale.getSellerId(),
                    sale.getDate(),
                    sale.getName(),
                    sale.getAmount());
        } else {
            return -1;
        }
    }
}

package com.yornel.sellers.service;

import com.yornel.sellers.model.Seller;
import com.yornel.sellers.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SellerService {

    @Autowired
    private SellerRepository repository;

    public List<Seller> getAll() {
        return repository.getAll();
    }

    public Seller findById(Long sellerId) {
        return repository.findById(sellerId).orElse(null);
    }
}

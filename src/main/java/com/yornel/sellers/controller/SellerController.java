package com.yornel.sellers.controller;

import com.yornel.sellers.model.Seller;
import com.yornel.sellers.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "sellers")
public class SellerController {

    @Autowired
    private SellerService service;

    @GetMapping
    public List<Seller> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Seller findById(@PathVariable("id") Long sellerId) {
        return service.findById(sellerId);
    }
}

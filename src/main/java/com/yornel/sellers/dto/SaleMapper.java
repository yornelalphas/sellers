package com.yornel.sellers.dto;

import com.yornel.sellers.model.Sale;
import com.yornel.sellers.util.DateUtils;

import java.util.Map;

public class SaleMapper {

    private SaleMapper() {

    }

    public static Sale mapToSale(Map<String, String> mapSale) {
        Sale sale = new Sale();
        sale.setSellerId(Long.valueOf(mapSale.get("sellerId")));
        sale.setName(mapSale.get("name"));
        sale.setAmount(Double.valueOf(mapSale.get("amount")));
        sale.setDate(DateUtils.toTimestamp(mapSale.get("date")));
        return sale;
    }
}

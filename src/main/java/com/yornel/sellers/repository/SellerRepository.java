package com.yornel.sellers.repository;

import com.yornel.sellers.model.Seller;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SellerRepository extends JpaRepository<Seller, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM seller")
    List<Seller> getAll();

    @Query(nativeQuery = true, value =
            "SELECT a.id, a.name, a.avatar, a.email, a.commission, a.active, total AS current_commissions " +
            "FROM seller a LEFT JOIN (SELECT *, SUM(commission_obtained) AS total FROM sale GROUP BY seller_id)b " +
            "ON a.id = b.seller_id WHERE a.id = :seller_id")
    Optional<Seller> findById(@Param("seller_id") Long sellerId);
}
